library(rlfsm)

##################################################
N_T <- 5e2
r_vect <- sample(c(1:20), size = 1e3, replace = TRUE)

t1<-Sys.time()
for (ind_j in 1:N_T) {

  increment(r=1,i=50,k=4,path=r_vect)

}
Sys.time()-t1


t1<-Sys.time()
for (ind_j in 1:N_T) {

  increments_Cpp(r=1,i=50,k=4,path=r_vect)

}
Sys.time() - t1
##################################################

N=1e3; m=4e0; M=1e2; alpha=1.4; H=0.3

t1<-Sys.time()
for (ind_j in 1:N_T) {

  vv_rm <- vector(mode='numeric', length = m*(M+N))
  vv_rm <- a_tilda(N=N, m=m, M=M, alpha=alpha, H=H)

}
Sys.time()-t1


t1<-Sys.time()

for (ind_j in 1:N_T) {
  vv<-vector(mode='double', length = m*(M+N))
  vv[seq(1, m*M, 1)]<-a_tilda_cpp(N=N, m=m, M=M, alpha=alpha, H=H)
  vv_n <- as.numeric(vv)

}
Sys.time() - t1
##################################################

N=1; M=2; m=1
a_tilda(N=N, m=m, M=M, alpha=alpha, H=H)== a_tilda_cpp(N=N, m=m, M=M, alpha=alpha, H=H)

N=1; M=1; m=1
a_tilda(N=N, m=m, M=M, alpha=alpha, H=H)== a_tilda_cpp(N=N, m=m, M=M, alpha=alpha, H=H)

N=1; M=1; m=2
a_tilda(N=N, m=m, M=M, alpha=alpha, H=H)== a_tilda_cpp(N=N, m=m, M=M, alpha=alpha, H=H)

N=10; M=2; m=1
a_tilda(N=N, m=m, M=M, alpha=alpha, H=H)== a_tilda_cpp(N=N, m=m, M=M, alpha=alpha, H=H)

N=1e2; M=4e1; m=10
round(a_tilda(N=N, m=m, M=M, alpha=alpha, H=H),4) ==
round(a_tilda_cpp(N=N, m=m, M=M, alpha=alpha, H=H),4)

##################################################

a_tilda_RCPP <- function(N, m, M,  alpha, H){
  
  vv<-vector(mode='double', length = m*(M+N))
  vv[seq(1, m*M, 1)]<-a_tilda_cpp(N=N, m=m, M=M, alpha=alpha, H=H)
  vv_n <- as.numeric(vv)
  
}


N=1e5; m=1e2; M=1e3; alpha=.4; H=0.3

t1<-Sys.time()
for (ind_j in 1:N_T) {
  
  
  vv_rm <- a_tilda(N=N, m=m, M=M, alpha=alpha, H=H)
  
}
Sys.time()-t1


t1<-Sys.time()

for (ind_j in 1:N_T) {
  
  vv_n <- a_tilda_RCPP(N=N, m=m, M=M, alpha=alpha, H=H)
  
}
Sys.time() - t1