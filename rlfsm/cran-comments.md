## New version of rlfsm package
This is an update for an existing package. In this version I fixed WARNINGS associated with the new format of documentation.

## Test environments
* local ubuntu 20.04, R-release

* r-devel-linux-x86_64-debian-gcc (on R-hub)
* r-devel-linux-x86_64-fedora-gcc (on R-hub)

* r-release-osx-x86_64 (on R-hub)
* r-devel-windows-ix86+x86_64 (on R-hub)

## R CMD check results
There were no ERRORs or WARNINGs or NOTEs.

## Downstream dependencies
There are currently no downstream dependencies for this package.
