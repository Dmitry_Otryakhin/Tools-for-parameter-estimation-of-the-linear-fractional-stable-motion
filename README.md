# Tools for parameter estimation of the linear fractional stable motion

[![CRAN_Status_Badge](https://www.r-pkg.org/badges/version/rlfsm)](https://cran.r-project.org/package=rlfsm)
[![](https://cranlogs.r-pkg.org/badges/grand-total/rlfsm?color=orange)](https://cran.r-project.org/package=rlfsm)
[![](https://cranlogs.r-pkg.org/badges/rlfsm)](https://cran.r-project.org/package=rlfsm)

Package rlfsm is implementation of the simulation method for linear fractional stable motion developed by Stoev and Taqqu (2004), as well as statistical inference procedures described in Mazur et al (2018). There is also an attempt to create a common coding standard for stochastic processes driven by Levy motions, which is though in a very early stage.

## Installation
``` r
# Stable version on CRAN:
install.packages("rlfsm")

# The latest development version:
install.packages("devtools")

s<-"https://gitlab.com/"
u<-"Dmitry_Otryakhin/"
r<-"Tools-for-parameter-estimation-of-the-linear-fractional-stable-motion"
sd<-"rlfsm"
path<-paste(s,u,r,sep="")
devtools::install_git(url=path, subdir = sd)
```

## Documentation
There is a standard vignette available. Apart from that, a paper on the package structure and numerical aspects of the LFSM could be found in [The R Journal](https://journal.r-project.org/archive/2020/RJ-2020-008/).

## Feedback and contributing
* If you have any useful comments, please consider leaving feedback [here](https://gitlab.com/Dmitry_Otryakhin/Tools-for-parameter-estimation-of-the-linear-fractional-stable-motion/issues/1).

* For bug reports, please open an [issue](https://gitlab.com/Dmitry_Otryakhin/Tools-for-parameter-estimation-of-the-linear-fractional-stable-motion/issues).

## Acknowledgments
The authors acknowledge financial support from the project ”Ambit fields: probabilistic properties and statistical inference” funded by Villum Fonden.
