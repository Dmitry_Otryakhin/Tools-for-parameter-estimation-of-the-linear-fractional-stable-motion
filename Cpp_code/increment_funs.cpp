#include <Rcpp.h>
#include <iostream>
#include <vector>
#include <cmath>
using namespace std;
using namespace Rcpp;


int nChoosek( int n, int k )
{
  if (k > n) return 0;
  if (k * 2 > n) k = n-k;
  if (k == 0) return 1;
  
  int result = n;
  for( int i = 2; i <= k; ++i ) {
    result *= (n-i+1);
    result /= i;
  }
  return result;
}


// [[Rcpp::export]]
double increments_Cpp(int i, int k, int r, vector<double> path) {
  
  double  S=0;
  for (int j = 0; j<k+1; j=j+1)
  {
    S = S + pow(-1,j) * nChoosek(k,j) * path[i-r*j];
  }
  return S;
}


/*** R

increment1<-function(r,i,k,path) {
  
  if (sum(i<r*k)) stop("i must be greater or equal to r*k") else
  {
    increments_Cpp(i, k, r, path)
  }
}

r_vect <- sample(c(1:20), size = 1e4, replace = TRUE)

increments_Cpp(r=1, i=40, k=4, path=r_vect)
increment1(r=1, i=40, k=4, path=r_vect)
increment(r=1, i=40, k=4, path=r_vect)

*/
