#### Check reasonableness of introducing rnorm in path function when alpha ####


delta<-1e3
Ntest<-seq(from=delta, to=2e4, by=delta)


# if performance
ts1<-Sys.time()

if(2==2) {
    #x<-1
} else {
    #x<-4
}

delta_if=Sys.time()-ts1
delta_if


# rnorm timing function
Norm_time<-function(N){
    
    ts1<-Sys.time()
    x<-rnorm(N)
    delta_norm=Sys.time()-ts1
    delta_norm
}
norm_times<-sapply(Ntest,Norm_time)


# rstable timing function
Stab_time<-function(N){
    
    ts1<-Sys.time()
    y<-rstable(N,alpha=2,beta=0)
    delta_stable=Sys.time()-ts1
    delta_stable
}
stab_times<-sapply(Ntest,Stab_time)

plot(y=norm_times+as.numeric(delta_if), x=Ntest, pch=6, col='green')
points(y=stab_times, x=Ntest, pch=7, col='blue')
legend("topright",legend = c('norm', 'stable'), col=c('green','blue'), pch = c(6,7))


#### Monte-Carlo ####
Nrep<-100
datat<-data.frame(stringsAsFactors = FALSE)

for(ind in Ntest) {
    
    datat<-rbind(datat,
                cbind(replicate(Nrep, Norm_time(ind)+delta_if, simplify = "array"),ind,'normal + delta_if',stringsAsFactors=FALSE),
                cbind(replicate(Nrep, Stab_time(ind), simplify = "array"),ind,'stable',stringsAsFactors=FALSE), 
                stringsAsFactors=FALSE)
}
colnames(datat)<-c('time','N','dist')
datat$time<-as.numeric(datat$time)
datat$N<-as.numeric(datat$N)
datat$dist<-as.factor(datat$dist)
datat<-datat[,1:3]
D<-ddply(datat, .(N, dist), summarize,
      mean = mean(time),
      low5 = quantile(time, probs =.05),
      high5 = quantile(time, probs =.95))
    
# Plot the data
p<- ggplot(D, aes(x=N, y=mean, group=dist, color=dist)) + 
    geom_line() +
    geom_point()+
    geom_errorbar(aes(ymin=low5, ymax=high5), width=.2,
                  position=position_dodge(0.05))


p
ggsave(file='speed.pdf',device='pdf')
